package receive

import (
	"gitee.com/jhx1219/machine-go/utils/common/task"
)

type Receive struct {
	Task *task.Task
}

func NewReceive(t *task.Task) *Receive {

	t.SetTaskFunc(func(msg string, task *task.Task) {
		//log.Println(msg)
	})

	return &Receive{
		Task: t,
	}
}

func (r *Receive) SetTaskFunc(f func(string, *task.Task)) {
	r.Task.SetTaskFunc(f)
}

func (r *Receive) LogReceice(msg string) error {

	err := r.Task.LogTask(msg)

	if err != nil {
		return err
	}

	return nil
}

func (r *Receive) ReceiveTaskServer() {
	r.Task.TaskServer()
}
