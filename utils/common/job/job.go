package job

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"

	"gitee.com/jhx1219/machine-go/utils/common/task"
)

type JobConf struct {
	JobList []JobListConf `json:"job_list"`
}

type JobListConf struct {
	Name string `json:"name"`
}

type JobInfo struct {
	Task *task.Task
}

type Job struct {
	JobLogPath string
	JobMap     map[string]*JobInfo
}

func NewJob() *Job {

	job := &Job{}
	job.JobLogPath = os.Getenv("JOB_LOG_PATH")
	job.JobMap = make(map[string]*JobInfo)

	confPath := os.Getenv("CONF_PATH")

	data, err := ioutil.ReadFile(confPath + "/job.json")
	if err != nil {
		log.Println("读取job文件失败: " + err.Error())
		return job
	}

	// 解析 JSON 数据
	var jobConf JobConf
	err = json.Unmarshal(data, &jobConf)
	if err != nil {
		log.Println("解析job文件失败: " + err.Error())
		return job
	}

	for _, conf := range jobConf.JobList {
		t := task.NewTask(job.JobLogPath + "/" + conf.Name)
		job.JobMap[conf.Name] = &JobInfo{
			Task: t,
		}
	}

	return job
}

func (j *Job) JobTaskServer() {
	for _, job := range j.JobMap {
		jobCopy := job
		jobCopy.Task.TaskServer()
	}
}

func (j *Job) LogJobTask(msg string) {
	for _, job := range j.JobMap {
		job.Task.LogTask(msg)
	}
}

func (j *Job) SetJobFunc(job string, f func(string, *task.Task)) {
	if _, ok := j.JobMap[job]; !ok {
		return
	}

	j.JobMap[job].Task.TaskFunc = f
}
